const mongoose = require('mongoose');
const keys = require('../../config');
const winston = require('winston')
var Fawn = require("fawn");





module.exports = function () {


    // initializing Fawn for future multiple transactions
    Fawn.init(mongoose);

    // database connecting....
    mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true })
        .then(() => { winston.info('connected to mongodb') })
        .catch((err) => { console.log('cannot connect to mongoDB') })


}