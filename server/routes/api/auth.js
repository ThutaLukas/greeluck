const express = require('express');
const passport = require('passport');
const router = express.Router();
const keys = require('../../config/index');
const { User } = require('../../models/User');
const jwt = require('jsonwebtoken');



// FACEBOOK OAUTH2 ROUTE
router.get('/facebook', passport.authenticate('facebook', {
    session: false,
    scope: ['email']

}));

router.get(
    '/facebook/callback',

    passport.authenticate('facebook', { session: false, failureRedirect: '/api/v1/users/login' }),
    async (req, res) => {


        const payload = req.user._id.toHexString();

        jwt.sign(
            payload,
            'yoursecret',
            (err, token) => {

                // if 'jwt' cookie is already in client . just send this token in cookie and if not send new 
                console.log(token);
                if (req.user.token) {
                    // res.cookie('auth' , req.user.token , {httpOnly : false  , maxAge :365 * 24 * 60 * 60 * 1000 })
                    res.send({ token: token, loginSuccess: true })
                } else {
                    req.user.token = token;
                    req.user.save((err, user) => {
                        console.log('success')
                    })
                    // res.cookie('auth' , token , {httpOnly : false  , maxAge :365 * 24 * 60 * 60 * 1000 })
                    res.send({ token: token, loginSuccess: true })
                }



                res.redirect('/user/dashboard')


            }

        )




    }

);







module.exports = router;