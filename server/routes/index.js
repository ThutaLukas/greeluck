const passport = require('passport');
const express = require('express');
const cookieParser = require('cookie-parser')



// custom own routes
const userRoutes = require('./api/user')
const authRoutes = require('./api/auth')
const productRoutes = require('./api/products')



module.exports = function (app) {
    // third party middlewares 
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(passport.initialize())
    app.use(cookieParser());


    // api middlewares 

    app.use('/api/v1/users', userRoutes)
    app.use('/api/v1/products', productRoutes)
    app.use('/auth', authRoutes)

}