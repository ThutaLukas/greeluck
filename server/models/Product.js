const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const productSchema = new Schema({

    name: {
        type: String
    },

    price: {
        promo: {
            type: Number
        },
        normal: {
            type: Number
        }
    },

    sku: {
        type: String

    },
    category: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category'
    },

    description: {
        type: String
    },

    stocks: {
        type: Number,
        default: 0
    },

    likes: {
        type: Number,
        default: 0
    },
    photos: [{ type: String }],

    details: new Schema({
        colors: {
            type: Array,
            default: []
        },
        size: {
            type: Array,
            default: []
        },
        featured: {
            type: Boolean
        },

        newArrival: {
            type: Boolean
        },

        promotional: {
            type: Boolean
        },

        sold: {
            type: Boolean
        }
    }),

    reviews: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'ProductReview'
        }
    ],
    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }

})





const Product = mongoose.model('Product', productSchema);
exports.Product = Product; 