import axios from 'axios';
import authService from '../auth/authservice'
import authservice from '../auth/authservice';

class AxiosService {


    axiosInstance = {}

    constructor() {
        this.initInstance()
    }


    initInstance() {


        this.axiosInstance = axios.create({

            baseURL: `/api/v1`,
            timeout: 6000

        })


        this.axiosInstance.interceptors.request.use(

            (config) => {
                // get token to set 'Authorization' header for send back to server
                const token = authservice.getToken();
                if (token) {
                    config.headers.Authorization = `Bearer ${token}`
                }

                return config
            }
        )

        return this.axiosInstance;
    }

    getInstance() {

        return this.axiosInstance || this.initInstance()

    }


}





export default new AxiosService();