export const LOGIN_USER = 'login_user';
export const REGISTER_USER = 'register_user';
export const LOGOUT_USER = 'logout_user';
export const AUTH_USER = 'auth_user';
export const USER_SERVER = '/api/v1/users';



export const ASYNC_ACTION_START = "ASYNC_ACTION_START";
export const ASYNC_ACTION_FINISH = "ASYNC_ACTION_FINISH"
export const ASYNC_ACTION_ERROR = "ASYNC_ACTION_ERROR"