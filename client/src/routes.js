
import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Auth from './components/hoc/Auth'

import Header from './components/landing/Header'
//import MainLayout from './components/Layouts/MainLayout'


import AdminLogin from './components/admin/admin-login/Admin-Login'
import AdminLayout from './components/admin/manage-admin/AdminLayout/AdminLayout'




const Routes = () => {





  return (



    <BrowserRouter>


      <Switch>

        <Route exact component={Header} path="/" />
        <Route exact component={AdminLogin} path="/login-admin" />


        {/* 
          admin dashboard related routes  
        */}
        <AdminLayout />



      </Switch>






    </BrowserRouter>
  )
}




export default Routes;

