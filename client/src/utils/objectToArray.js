export const objectToArray = (obj) => {

    if(obj) {
        return Object.entries(obj).map((e) => {
           return  Object.assign(e[1] , {id : e[0]})
        })
    }

}
