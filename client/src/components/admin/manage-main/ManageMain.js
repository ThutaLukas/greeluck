import React, { Component, Fragment } from 'react'


class ManageMain extends Component {






  render() {



    return (
      <section className=" px-5 py-3">
        <div className="container">
          <div className="row">
            <div className="col-xl-10 col-lg-9  ml-auto">
              <div className="row">
                <div className="col-xl-5 col-sm-6 p-2">
                  <div className="card card-common">
                    <div className="card-body">
                      <div className="d-flex justify-content-between">
                        <i className="fa fa-shopping-cart fa-3x text-success"></i>
                        <div className="text-right text-secondary">
                          <h5>Orders</h5>
                          <h3>100</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <i className="fa fa-sync mr-3"></i>
                      <button className="btn btn-success">Check Details</button>
                    </div>
                  </div>
                </div>
                <div className="col-xl-5 col-sm-6 p-2">
                  <div className="card card-common">
                    <div className="card-body">
                      <div className="d-flex justify-content-between">
                        <i className="fa fa-adn fa-3x text-success"></i>
                        <div className="text-right text-secondary">
                          <h5>Products</h5>
                          <h3>76</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <i className="fa fa-sync mr-3"></i>
                      <button className="btn btn-success">Check Details</button>
                    </div>
                  </div>
                </div>
                <div className="col-xl-5 col-sm-6 p-2">
                  <div className="card card-common">
                    <div className="card-body">
                      <div className="d-flex justify-content-between">
                        <i className="fa fa-shopping-cart fa-3x text-success"></i>
                        <div className="text-right text-secondary">
                          <h5>Orders</h5>
                          <h3>100</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <i className="fa fa-sync mr-3"></i>
                      <button className="btn btn-success">Check Details</button>
                    </div>
                  </div>
                </div>

                <div className="col-xl-5 col-sm-6 p-2">
                  <div className="card card-common">
                    <div className="card-body">
                      <div className="d-flex justify-content-between">
                        <i className="fa fa-user fa-3x text-success"></i>
                        <div className="text-right text-secondary">
                          <h5>Users</h5>
                          <h3>250</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <i className="fa fa-sync mr-3"></i>
                      <button className="btn btn-success">Check Details</button>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>



        </div>
      </section>






    )
  }
}



export default ManageMain;