import React, { Component } from 'react'
import TextInput from '../../utils/form/TextInput'
import {Field , reduxForm} from 'redux-form'



 class AdminForm extends Component {
  

  
  
  
  
    render() {
   
   
   
      return (
      <form>
           
                <Field 
                    name="email"
                    component={TextInput}
                    type="text"
                    placeholder="enter your email"
                    
                    logo="ni ni-email-83"
                />


                  <Field
                    name="password"
                    component={TextInput}
                    type="password"
                    placeholder="enter your password"
                  
                    logo="ni ni-lock-circle-open"
                   />

                    <div className="text-center">
                        <button type="button" className="btn btn-success my-4 btn-block bg-gradient-success">Sign in</button>
                        </div>

           
      </form>



    )
  }
}




export default reduxForm({
    form : 'admin'
})(AdminForm)




{/* <form>
<div className="form-group mb-3">
  <div className="input-group input-group-alternative">
    <div className="input-group-prepend">
      <span className="input-group-text"><i className="ni ni-email-83"></i></span>
    </div>
    <input className="form-control" placeholder="Email" type="email"/>
  </div>
</div>
<div className="form-group">
  <div className="input-group input-group-alternative">
    <div className="input-group-prepend">
      <span className="input-group-text"><i className="ni ni-lock-circle-open"></i></span>
    </div>
    <input className="form-control" placeholder="Password" type="password"/>
  </div>
</div>

<div className="text-center">
  <button type="button" className="btn btn-success my-4">Sign in</button>
</div>
</form> */}