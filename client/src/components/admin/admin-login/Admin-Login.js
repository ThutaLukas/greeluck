import React, { Component } from 'react'
import AdminForm from './AdminForm'







 class AdminLogin extends Component {
 

  
  
  
    render() {
     return (

          <section className="">
                    <div className="header bg-gradient-success py-7 py-lg-8  ">
                        <div className="container-fluid py-5">
                            <div className="header-body text-center mb-7">
                              <div className="row justify-content-center">
                                <div className="col-lg-5 col-md-6">
                                  <h2 className="text-white display-2">Welcome to Green Luck Admin Page</h2>
                                  <p className="text-white my-5">This Login Page is only for GreenLuck Admins and don't share with others</p>
                                </div>
                              </div>
                            </div>
                        </div>

                                                    
                    </div>




                <div className="container py-5 my-5">
                    <div className="row justify-content-center">
                          
                            <div className="col-lg-5 col-md-7">
                              <div className="card bg-secondary shadow border-0">
                                          <div className="card-header bg-transparent pb-5">
                                            <div className="text-muted text-center mt-2 mb-3"><small>Sign in with</small></div>
                                            <div className="btn-wrapper text-center">
                                              <a href="#" className="btn btn-neutral btn-icon ">
                                                <span className="btn-inner--icon"><i className="fa fa-facebook-square"></i></span>
                                                <span className="btn-inner--text">Facebook</span>
                                              </a>
                                              <a href="#" className="btn btn-neutral btn-icon">
                                                <span className="btn-inner--icon"><i className="fa fa-google"></i></span>
                                                <span className="btn-inner--text">Google</span>
                                              </a>
                                            </div>
                                          </div>
                                        <div className="card-body px-lg-5 py-lg-5">
                                              <div className="text-center text-muted mb-4">
                                                <small>Or sign in with credentials</small>
                                              </div>

                                              <AdminForm/>



                                        </div>
                                      </div>
                            
                            </div>
                     </div>
            </div>

            
          </section>

     

    )
  }
}



export default AdminLogin