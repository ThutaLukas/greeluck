

export const sidebarContents = [

    {
        to: "/admin",
        name: "Home",
        logo: "fa-home",
        admin: false
    },
    {
        to: "/admin/orders",
        name: "Orders",
        logo: "",
        admin: true,

    },
    {
        to: "/admin/products",
        name: "Products",
        logo: "",
        admin: false,

    },
    {
        to: "/admin/users",
        name: "Users",
        logo: "",
        admin: true,

    },

    {
        to: "/admin/promotions",
        name: "Promotions",
        logo: "",
        admin: true,

    },
    {
        to: "/admin/others",
        name: "Others",
        logo: "",
        admin: true,

    },






]