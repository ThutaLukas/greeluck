import React, { Component, Fragment } from 'react'
import AdminMain from '../../manage-main/ManageMain'
import { Switch, Route, NavLink } from 'react-router-dom'
import AdminSideBar from '../AdminSideBar/AdminSideBar'


import Auth from '../../../hoc/Auth'



import ManageOrders from '../../manage-orders/ManageOrders'
import ManageProducts from '../../manage-products/index'


import styles from './AdminLayout.module.css';



class AdminLayout extends Component {













  render() {




    return (

      <Fragment>

        <nav className="navbar">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xl-2 col-lg-3  sidebar fixed-top">
                <AdminSideBar />
              </div>

              <div className={`col-xl-10 col-lg-9 col-md-8 ml-auto  fixed-top  top-navbar ${styles.navbarColor}`}>
                <div className=" p-2 d-flex justify-content-end">
                  <div className="">
                    <a href="#" className={`nav-link icon-bullet ${styles.iconBullet}`}><i className="fa fa-cog"></i></a>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </nav>



        <section className="py-4">

          <div className="container-fluid">
            <div className="row">

              <div className="col-xl-10 col-lg-9 ml-auto">
                <Switch>
                  <Route exact component={AdminMain} path="/admin" />
                  <Route exact component={ManageOrders} path="/admin/orders" />
                  <Route exact component={ManageProducts} path="/admin/products" />
                </Switch>

              </div>



            </div>

          </div>


        </section>
      </Fragment >
    )
  }
}



export default AdminLayout;