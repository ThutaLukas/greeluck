import React from 'react'

  




const TextInput =  ({input , width , type , placeholder , logo ,  meta : {touched , error } }) => {
   
   
   
   
    return (
        <div className="form-group mb-3">
            <div className="input-group input-group-alternative ">
            <div className="input-group-prepend">
                    <span className="input-group-text"><i className={logo}></i></span>
            </div>
            <input  className="form-control" {...input} placeholder={placeholder} type={type}/>
            </div>
         
        
                {touched && error && <label basic color="red">{error}</label>}
        </div>
    )
}

export default TextInput;